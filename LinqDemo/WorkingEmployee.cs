﻿using System.Collections.Generic;

namespace LinqDemo
{
    public class WorkingEmployee1
    {
        public string Place { get; set; }

        public IEnumerable<Employee> Persons { get; set; }  
    }

    public class WorkingEmployee2
    {
        public string Place { get; set; }

        public string Person { get; set; }
    }
}
