﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace LinqDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //            var persons = GeneratePersons();
            //            
            //            foreach (var person in persons)
            //            {
            //                Console.WriteLine($"{person.Name} - {person.Age}");
            //            }
            //
            //            var firstPerson = persons.Single(x => x.Age == 26);
            //            Console.WriteLine(firstPerson?.Name);

            ////Where
            //Console.WriteLine("Filtering using Where");
            //var filteredPersons = persons
            //    .Where(p => p.Age < 20);
            //foreach (var filteredPerson in filteredPersons)
            //{
            //    Console.WriteLine($"{filteredPerson.Name} - {filteredPerson.Age}");
            //}

            //Console.WriteLine("----");
            //persons.Add(new Person() { Address = null, Age = 4, Name = "test" });
            //foreach (var filteredPerson in filteredPersons)
            //{
            //    Console.WriteLine($"{filteredPerson.Name} - {filteredPerson.Age}");
            //}

            //            var anotherWayToFilter = from p in persons
            //                                     where p.Age < 20 &&
            //                                           p.Name == "Jane"
            //                                     select p;

            //foreach (var filteredPerson in anotherWayToFilter)
            //{
            //    Console.WriteLine($"{filteredPerson.Name} - {filteredPerson.Age}");
            //}

            ////Select
            //            Console.WriteLine("Projection");
            //            var projectedPersons = persons.Select(p => new
            //            {
            //                Name = p.Name,
            //                City = p.Address.City
            //            });
            //
            //            var anotherWayToFilter = from p in persons
            //                                select new
            //                                     {
            //                                         Name = p.Name,
            //                                         City = p.Address.City
            //                                     };

            //foreach (var projectedPerson in projectedPersons)
            //{
            //    Console.WriteLine($"{projectedPerson.Name} lives in {projectedPerson.City}");
            //}


            ////Group by
            //            var groupedResult = persons.GroupBy(x => x.Age);
            //
            //            foreach (var group in groupedResult)
            //            {
            //                Console.WriteLine($"Age: {group.Key}");
            //                foreach (var person in group)
            //                {
            //                    Console.WriteLine($"Name: {person.Name}");
            //                }
            //            }

            ////Take
            //Console.WriteLine("Take");
            //var takeDemoPersons = persons.Take(2);
            //foreach (var person in takeDemoPersons)
            //{
            //    Console.WriteLine($"{person.Name}");
            //}

            //Console.WriteLine();

            ////TakeWhile
            //Console.WriteLine("TakeWhile");
            //var takeWhilePersons = persons.TakeWhile(p => p.Age < 30);
            //foreach (var person in takeWhilePersons)
            //{
            //    Console.WriteLine($"{person.Name}");
            //}
            //Console.WriteLine();

            ////Skip
            //Console.WriteLine("Skip");
            //var skipDemoPersons = persons.Skip(2);
            //foreach (var person in skipDemoPersons)
            //{
            //    Console.WriteLine($"{person.Name}");
            //}
            //Console.WriteLine();

            ////SkipWhile
            //Console.WriteLine("SkipWhile");
            //var skipWhilePersons = persons.SkipWhile(p => p.Age < 30);
            //foreach (var person in skipWhilePersons)
            //{
            //    Console.WriteLine($"{person.Name}");
            //}
            //Console.WriteLine();

            ////OrderBy
            //Console.WriteLine("OrderBy");
            //var orderedPersons = persons.OrderByDescending(x => x, new PersonComparer());
            //foreach (var orderedPerson in orderedPersons)
            //{
            //    Console.WriteLine($"{orderedPerson.Name} - {orderedPerson.Age}");
            //}

            ////OrderByDescending
            //Console.WriteLine("OrderByDescending");
            //var orderedByDescendingPersons = persons.OrderByDescending(x => x.Name);
            //foreach (var orderedPerson in orderedByDescendingPersons)
            //{
            //    Console.WriteLine($"{orderedPerson.Name}");
            //}

            ////ThenBy
            //Console.WriteLine("ThenBy");
            //var thenByPersons = persons
            //    .OrderBy(x => x.Name)
            //    .ThenBy(x => x.Address.Street);
            //foreach (var orderedPerson in thenByPersons)
            //{
            //    Console.WriteLine($"{orderedPerson.Name} works on {orderedPerson.Address.Street}");
            //}

            //Console.WriteLine("ThenByMultiple");
            //var thenByMultiplePersons = persons
            //    .OrderBy(x => x.Name)
            //    .ThenBy(x => x.Address.Country)
            //    .ThenBy(x => x.Address.Street);
            //foreach (var orderedPerson in thenByMultiplePersons)
            //{
            //    Console.WriteLine($"{orderedPerson.Name} lives in {orderedPerson.Address.Country} and works on {orderedPerson.Address.Street}");
            //}

            ////ThenByDescending
            //Console.WriteLine("ThenByDescending");
            //var thenByDescendingPersons = persons
            //    .OrderBy(x => x.Name)
            //    .ThenByDescending(x => x.Address.Street);
            //foreach (var orderedPerson in thenByDescendingPersons)
            //{
            //    Console.WriteLine($"{orderedPerson.Name} works on {orderedPerson.Address.Street}");
            //}

            //Console.WriteLine();

            //Generations
            //            var intEnumerable = Enumerable.Range(3, 20);
            //            Console.WriteLine("Range: ");
            //            foreach (var i in intEnumerable)
            //            {
            //                Console.WriteLine(i);
            //            }
            //            Console.WriteLine();

            //            var repeatedEnumerable = Enumerable.Repeat("2", 10);
            //            Console.WriteLine("Repeat: ");
            //            foreach (var i in repeatedEnumerable)
            //            {
            //                Console.WriteLine(i);
            //            }
            //            Console.WriteLine();

            //            var emptyEnumerable = Enumerable.Empty<int>();
            //            Console.WriteLine("Empty: ");
            //            foreach (var element in emptyEnumerable)
            //            {
            //                Console.WriteLine(element);
            //            }
            //            Console.WriteLine();

            //Aggregate operations
            //            var sum = intEnumerable.Sum();
            //            Console.WriteLine($"Sum: {sum}");
            //            Console.WriteLine();
            //
            //            var ageSum = persons.Sum(p => p.Age);
            //            Console.WriteLine($"Sum of persons' ages: {ageSum}");
            //            Console.WriteLine();
            //
            //            var count = intEnumerable.Count();
            //            Console.WriteLine($"Count: {count}");
            //            Console.WriteLine();
            //
            //            var evenCount = intEnumerable.Count(x => x % 2 == 0);
            //            Console.WriteLine($"Contains {evenCount} even numbers");
            //            Console.WriteLine();
            //
            //            var max = intEnumerable.Max();
            //            Console.WriteLine($"Max: {max}");
            //            Console.WriteLine();
            //
            //            var maxAge = persons.Max(p => p.Age);
            //            Console.WriteLine($"Maximum age: {maxAge}");
            //            Console.WriteLine();
            //
            //            var min = intEnumerable.Min();
            //            Console.WriteLine($"Min: {min}");
            //            Console.WriteLine();
            //
            //            var minAge = persons.Min(p => p.Age);
            //            Console.WriteLine($"Minimum age: {minAge}");
            //            Console.WriteLine();
            //
            //            var average = intEnumerable.Average();
            //            Console.WriteLine($"Average: {average}");
            //            Console.WriteLine();
            //
            //            var averageAge = persons.Average(p => p.Age);
            //            Console.WriteLine($"Average age: {averageAge}");
            //            Console.WriteLine();

            //            var anotherIntEnumerable = Enumerable.Range(1, 5);
            //            var aggregate = anotherIntEnumerable.Aggregate((accumulator, element) => accumulator * element);
            //            Console.WriteLine($"Aggregated result: {aggregate}");
            //            Console.WriteLine();

            //Set operations
            //            var intList = new List<int> { 1, 2, 3, 4 };
            //            var anotherIntList = new List<int> { 4, 3, 5, 6 };
            //            Console.WriteLine();

            //            var union = intList.Union(anotherIntList);
            //            var intersect = intList.Intersect(anotherIntList);
            //            var except = union.Except(intersect);
            //
            //            var result = intList.Union(anotherIntList).Except(intList.Intersect(anotherIntList));
            //
            //            var result2 = intList.Except(anotherIntList).Union(anotherIntList.Except(intList));
            //
            //            foreach (var element in result2)
            //            {
            //                Console.WriteLine(element);
            //            }

            //            Console.WriteLine("Concat");
            //            var concat = intList.Concat(anotherIntList);
            //            foreach (var element in concat)
            //            {
            //                Console.WriteLine(element);
            //            }
            //            Console.WriteLine();

            //            Console.WriteLine("Union");
            //            var union = anotherIntList.Union(intList);
            //            foreach (var element in union)
            //            {
            //                Console.WriteLine(element);
            //            }
            //            Console.WriteLine();
            ////
            //            Console.WriteLine("Intersect");
            //            var intersect = anotherIntList.Intersect(intList);
            //            foreach (var element in intersect)
            //            {
            //                Console.WriteLine(element);
            //            }
            //            Console.WriteLine();
            //
            //            Console.WriteLine("Except");
            //            var except = anotherIntList.Except(intList);
            //            foreach (var element in except)
            //            {
            //                Console.WriteLine(element);
            //            }
            //            Console.WriteLine();

            //            Console.WriteLine("Contains");
            //            var contains = intList.Contains(3);
            //            Console.WriteLine(contains);
            //            Console.WriteLine();
            //
            //            var s = "Contains";
            //            var result = s.Contains("tai");
            //            Console.WriteLine(result);
            //
            //            var list = Enumerable.Empty<int>();
            //            Console.WriteLine("All");
            //            var all = list.All(i => i < 3);
            //            Console.WriteLine(all);
            //            Console.WriteLine();
            //
            //            Console.WriteLine("Any");
            //            var any = list.Any(i => i < 3);
            //            Console.WriteLine(any);
            //            Console.WriteLine();

            //            var persons1 = new List<Employee>
            //            {
            //                new Employee
            //                {
            //                    Name = "John",
            //                    AddressId = 1
            //                },
            //                new Employee
            //                {
            //                    Name = "Michael",
            //                    AddressId = 2
            //                }
            //            };
            //            
            //            var persons2 = new List<Employee>
            //            {
            //                new Employee
            //                {
            //                    Name = "John",
            //                    AddressId = 3
            //                }
            //            };
            //            
            //            var exceptWithComparer = persons1.Except(persons2, new EmployeeComparer());
            //            foreach (var employee in exceptWithComparer)
            //            {
            //                Console.WriteLine($"Name - {employee.Name}; AddressId = {employee.AddressId}");
            //            }

            //            var intSequence = new List<int> { 1, 2, 3, 4 };
            //            var anotherIntSequence = new List<int> { 1, 2, 3, 4 };
            //            Console.WriteLine("SequenceEquals");
            //            var sequenceEquals = intSequence.SequenceEqual(anotherIntSequence);
            //            Console.WriteLine(sequenceEquals);
            //            Console.WriteLine();

            //            //Join
            //            var addresses = GenerateAddresses();
            //            var employees = GenerateEmployees();
            //            var result = (from a in addresses
            //                join e in employees on a.Id equals e.AddressId
            //                into groupedEmployees
            //                from g in groupedEmployees.DefaultIfEmpty()
            //                select new { g.Street, g.Name });

            //            var joinedResult = employees.Join(
            //                addresses,
            //                e => e.AddressId,
            //                a => a.Id,
            //                (employee, address) => new WorkingEmployee
            //                {
            //                    Place = $" {address?.Street}, {address?.City}",
            //                    Person = employee.Name
            //                });
            //            //from a in addresses
            //            //    join e in employees 
            //
            //            foreach (var r in result)
            //            {
            //                Console.WriteLine($"{r.Street} works at {r.Name}");
            //            }

            //Console.WriteLine();

            //            //GroupJoin
            //            var groupJoinedResult = addresses.GroupJoin(
            //                employees,
            //                a => a.Id,
            //                e => e.AddressId,
            //                (address, workingEmployees) => new WorkingEmployee1
            //                {
            //                    Place = $" {address.Street}, {address.City}",
            //                    Persons = workingEmployees
            //                });
            //            foreach (var result in groupJoinedResult)
            //            {
            //                Console.WriteLine($"Working address: {result.Place}");
            //                foreach (var employee in result.Persons)
            //                {
            //                    Console.WriteLine($"\t- {employee.Name}");
            //
            //                }
            //            }
            //            Console.WriteLine();

            //            //Zip
            //            var numbers1To5 = Enumerable.Range(1, 7);
            //            var zippedResult = employees.Zip(
            //                numbers1To5,
            //                (employee, number) => $"{number} - {employee.Name}");
            //            foreach (var result in zippedResult)
            //            {
            //                Console.WriteLine(result);
            //            }

            //Left join
            //            var query = from add in addresses
            //                join emp in employees on add.Id equals emp.AddressId into empAdd
            //                from a in empAdd.DefaultIfEmpty()
            //                select new { add.Street, EmployeeName = a?.Name ?? String.Empty };
            //
            //            foreach (var q in query)
            //            {
            //                Console.WriteLine(q);
            //            }
            //            Console.WriteLine();
        }

        private static IEnumerable<Address> GenerateAddresses()
        {
            var zh75Address = new Address
            {
                Id = 1,
                City = "Kyiv",
                Country = "Ukraine",
                PostalCode = 40000,
                Street = "Zhylianska"
            };
            var f30Address = new Address
            {
                Id = 2,
                City = "Kiyv",
                Country = "Ukraine",
                PostalCode = 30000,
                Street = "Fizkultury"
            };
            var nyAddress = new Address
            {
                Id = 3,
                City = "NY",
                Country = "USA",
                PostalCode = 20000,
                Street = "Broadway"
            };
            return new List<Address>
            {
                zh75Address,
                f30Address,
                nyAddress
            };
        }

        private static List<Employee> GenerateEmployees()
        {
            return new List<Employee>
            {
                new Employee
                {
                    Name = "John",
                    AddressId = 1
                },
                new Employee
                {
                    Name = "Jane",
                    AddressId = 2
                },
                new Employee
                {
                    Name = "Frank",
                    AddressId = 3
                },
                new Employee
                {
                    Name = "Anna",
                    AddressId = 2
                },
                new Employee
                {
                    Name = "Mark",
                    AddressId = 3
                }
            };
        }

        private static List<Person> GeneratePersons()
        {
            var zh75Address = new Address
            {
                Id = 1,
                City = "Kyiv",
                Country = "Ukraine",
                PostalCode = 40000,
                Street = "Zhylianska"
            };
            var f30Address = new Address
            {
                Id = 2,
                City = "Kyiv",
                Country = "Ukraine",
                PostalCode = 30000,
                Street = "Fizkultury"
            };
            var nyAddress = new Address
            {
                Id = 3,
                City = "NY",
                Country = "USA",
                PostalCode = 20000,
                Street = "Broadway"
            };
            return new List<Person>
            {
                new Person
                {
                    Age = 3,
                    Name = "John Doe",
                    Address = nyAddress
                },
                new Person
                {
                    Age = 25,
                    Name = "Anna",
                    Address = zh75Address
                },
                new Person
                {
                    Age = 36,
                    Name = "Andrii",
                    Address = zh75Address
                },
                new Person
                {
                    Age = 25,
                    Name = "Mykola",
                    Address = f30Address
                }
            };
        }
    }
}
