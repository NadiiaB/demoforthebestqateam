﻿using System;
using System.Collections.Generic;

namespace LinqDemo
{
    public class Person : IComparable<Person>
    {
        public string Name { get; set; }

        public int Age { get; set; }

        public Address Address { get; set; }

        public int CompareTo(Person other)
        {
            return this.Age == other.Age ? 1 : -1;
        }
    }

    public class PersonComparer : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            if (x.Age == y.Age)
            {
                return 0;
            }
            if (x.Age > y.Age)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }
}
